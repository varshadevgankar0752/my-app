package com.hashcode;

import java.util.Objects;

public class InterStudent {
    private int Rollno;
    private String S_name;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InterStudent that = (InterStudent) o;
        return Rollno == that.Rollno && S_name.equals(that.S_name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Rollno, S_name);
    }

    public int getRollno() {
        return Rollno;
    }

    public void setRollno(int rollno) {
        Rollno = rollno;
    }

    public String getS_name() {
        return S_name;
    }

    public void setS_name(String s_name) {
        S_name = s_name;
    }
}
