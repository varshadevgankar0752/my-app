package com.hashcode;

import java.util.List;
import java.util.Objects;

public class StudentCollege {
    private String clg_name;
    private String address;
    List<StudentStandard> no_of_class;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentCollege that = (StudentCollege) o;
        return clg_name.equals(that.clg_name) && address.equals(that.address) && no_of_class.equals(that.no_of_class);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clg_name, address, no_of_class);
    }

    public List<StudentStandard> getNo_of_class(StudentStandard ss) {
        return no_of_class;
    }

    public void setNo_of_class(List<StudentStandard> no_of_class) {
        this.no_of_class = no_of_class;
    }

    public String getClg_name() {
        return clg_name;
    }

    public void setClg_name(String clg_name) {
        this.clg_name = clg_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}
