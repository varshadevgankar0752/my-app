package com.hashcode;

import java.util.Objects;
import java.util.Set;

public class StudentStandard {
    private int s_standard;
    private int total_students;
    Set<InterStudent>student_list;

    public Set<InterStudent> getStudent_list() {
        return student_list;
    }

    public void setStudent_list(Set<InterStudent> student_list) {
        this.student_list = student_list;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentStandard that = (StudentStandard) o;
        return s_standard == that.s_standard && total_students == that.total_students && Objects.equals(student_list, that.student_list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(s_standard, total_students, student_list);
    }

    public int getS_standard() {
        return s_standard;
    }

    public void setS_standard(int s_standard) {
        this.s_standard = s_standard;
    }

    public int getTotal_students() {
        return total_students;
    }

    public void setTotal_students(int total_students) {
        this.total_students = total_students;
    }


}
