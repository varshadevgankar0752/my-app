package com.myjava.strategypattern.strategypattern;

public abstract class Duck implements  FlyBehave,QuackBehave{

    FlyBehave flybe;
    QuackBehave quackbe;

    public abstract void display();

    public void fly(){
        System.out.println("i can fly");
    }
    public void quack(){
        System.out.println("Quack...!");
    }
    public void noFly(){
        System.out.println("i cannot fly...");
    }
    public void noQuack(){
        System.out.println(" i cannot Quack... ");
    }

public static void main(String args[]){
    WhiteDuck wd= new WhiteDuck();
    wd.display();
    wd.fly();
    wd.quack();
    BrowneDuck bn = new BrowneDuck();
    bn.display();
    bn.fly();
    bn.quack();
    MullardDuck md= new MullardDuck();
    md.display();
    md.fly();
    md.quack();
    RubberDuck rd = new RubberDuck();
    rd.display();
    rd.quack();
    DecoyDuck dd = new DecoyDuck();
    dd.display();
    dd.noFly();
    dd.noQuack();
}
}
class WhiteDuck extends Duck
{
    @Override
    public void display() {
        System.out.println("Iam WhiteDuck");
    }
}
class BrowneDuck extends Duck
{
    @Override
    public void display() {
        System.out.println("Iam BrownDuck");
    }
}
class MullardDuck extends Duck
{
    @Override
    public void display() {
        System.out.println("Iam MullardDuck");
    }
}
class RubberDuck extends Duck
{
    @Override
    public void display() {
        System.out.println("Iam RubberDuck");
    }

}
class DecoyDuck extends Duck
{
    @Override
    public void display() {
        System.out.println("Iam DecoyDuck");
    }

}
