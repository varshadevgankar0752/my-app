package com.myjava.strategypattern.strategypattern;

public interface QuackBehave {
    public void quack();
}
